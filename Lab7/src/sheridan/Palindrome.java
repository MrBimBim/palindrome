package sheridan;

public class Palindrome {

	public static Boolean isPalindrome(String palindrome)
	{
		palindrome = palindrome.toLowerCase().replaceAll(" ", "");
		
		for (int i =0, j= palindrome.length()-1; i < j; i++, j--)
		{
			if ( palindrome.charAt(i) != palindrome.charAt(j))
				return false;
		}
		return true;
	}
	public static void main(String[]args)
	{
		System.out.println("is anna palindrome " + Palindrome.isPalindrome("anna"));
		System.out.println("is anna palindrome " + Palindrome.isPalindrome("race car"));
		System.out.println("is anna palindrome " + Palindrome.isPalindrome("taco cat"));
		System.out.println("is anna palindrome " + Palindrome.isPalindrome("this is not"));
	}
}
